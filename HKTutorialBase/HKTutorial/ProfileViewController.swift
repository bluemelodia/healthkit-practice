//
//  ProfileViewController.swift
//  HKTutorial
//
//  Created by ernesto on 18/10/14.
//  Copyright (c) 2014 raywenderlich. All rights reserved.
//

import UIKit
import HealthKit

class ProfileViewController: UITableViewController {
  
  let UpdateProfileInfoSection = 2
  let SaveBMISection = 3
  let kUnknownString   = "Unknown"
  let healthKitStore:HKHealthStore = HKHealthStore()
  
  @IBOutlet var ageLabel:UILabel!
  @IBOutlet var bloodTypeLabel:UILabel!
  @IBOutlet var biologicalSexLabel:UILabel!
  @IBOutlet var weightLabel:UILabel!
  @IBOutlet var heightLabel:UILabel!
  @IBOutlet var bmiLabel:UILabel!
  
  var healthManager:HealthManager?
  var bmi:Double?
  var height, weight:HKQuantitySample? // use these two HKQuantitySample properties to read the weight and height samples from the HealthStore
  
  func updateHealthInfo() {
    
    updateProfileInfo();
    updateWeight();
    updateHeight();
    
  }
  
  // invoked when you click on the button Read HealthKit Data
  func updateProfileInfo()
  {
    // invokes readProfile()
    let profile = healthManager?.readProfile()
    if profile?.age != nil {
      ageLabel.text = String(profile!.age!)
    } else {
      ageLabel.text = "Unknown"
    }
    // convenience methods that return a string based on the numerica value of the blood type and biological sex
    biologicalSexLabel.text = biologicalSexLiteral(profile?.biologicalsex?.biologicalSex)
    bloodTypeLabel.text = bloodTypeLiteral(profile?.bloodtype?.bloodType)
  }
  
  // similar to updateWeight, except that the height sample type is constructed with the associated height type identifier HKQuantityTypeIdentifierHeight to allow you to read those samples; and it uses an NSLengthFormatter to get a localized string from the height value, which is used to get localized strings for lengths
  func updateHeight()
  {
    // 1) construct an HKSampleType for height 
    let sampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
    
    // 2) call the method to read the most recent height sample
    self.healthManager?.readMostRecentSample(sampleType!, completion: { (mostRecentHeight, error) -> Void in
      if(error != nil) {
        print("Error reading height from the HealthKit Store: \(error.localizedDescription)")
        return;
      }
      
      var heightLocalizedString = self.kUnknownString;
      self.height = mostRecentHeight as? HKQuantitySample;
      
      // 3) format the height to display it on the screen 
      if let meters = self.height?.quantity.doubleValueForUnit(HKUnit.meterUnit()) {
        let heightFormatter = NSLengthFormatter()
        heightFormatter.forPersonHeightUse = true;
        heightLocalizedString = heightFormatter.stringFromMeters(meters);
      }
      
      // 4) update UI. HealthKit use an internal queue, we make sure that we interact with the UI in the main thread 
      dispatch_async(dispatch_get_main_queue(), {() -> Void in
        self.heightLabel.text = heightLocalizedString
        self.updateBMI()
      });
    })
  }
  
  func updateWeight()
  {
    // 1) construct an HKSampleType for weight - specify the type of quantity sample you want to read by using quantityTypeForIdentifier (from HKSample), and you pass the ID associated to the weight type: HKQuantityTypeIdentifierBodyMass
    let sampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
    
    // 2) call the method to read the most recent weight sample (the method we created in HealthManager)
    self.healthManager?.readMostRecentSample(sampleType!, completion: {(mostRecentWeight, error) -> Void in
      if(error != nil) {
        print("Error reading weight from the HealthKit Store: \(error.localizedDescription)")
        return;
      }
      
      var weightLocalizedString = self.kUnknownString;
      // 3) format the weight to display it on the screen - in the completion closure, we get the weight sample value in kg using doubleValueForUnit, and then use a NSMassFormatter to transform that value into a localized text string
      self.weight = mostRecentWeight as? HKQuantitySample;
      if let kilograms = self.weight?.quantity.doubleValueForUnit(HKUnit.gramUnitWithMetricPrefix(.Kilo)) {
        let weightFormatter = NSMassFormatter() // converts quantities into text strings and takes the user's location into account - when you use it, you free yourself from localizing the strings or configuring the units of the current locale, even if your system isn't configured to use the metric system that you're using
        weightFormatter.forPersonMassUse = true;
        weightLocalizedString = weightFormatter.stringFromKilograms(kilograms)
      }
      
      // 4) Update UI in the main thread to display the weight - HealthKit uses an internal thread, so it's important to make sure that all UI updates happen on the main thread - we also call a method called updateBMI, which is included in the starter project ot calculate the BMI
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        self.weightLabel.text = weightLocalizedString
        self.updateBMI()
      });
    });
  }
  
  func updateBMI()
  {
    if weight != nil && height != nil {
      // 1) get the weight and height values from the samples read from HealthKit
      let weightInKilograms = weight!.quantity.doubleValueForUnit(HKUnit.gramUnitWithMetricPrefix(.Kilo)) // HKUnit provides methods to construct all the supported units, and here you've used grams for weight, meters for height - be careful about using compatible units, because if the requested unit is not compatible with the sample type, it'll raise an exception - ex. requesting a weight value using a distance unit won't work
      let heightInMeters = height!.quantity.doubleValueForUnit(HKUnit.meterUnit())
      
      // 2) call the method to calculate the BMI, a utility method included in the starter project that calculates BMI from weight and height
      bmi = calculateBMIWithWeightInKilograms(weightInKilograms, heightInMeters: heightInMeters)
    }
    // 3) show the calculated BMI in the corresponding label - since BMI is just a number, you don't need any formatter to do that
    var bmiString = kUnknownString
    if bmi != nil {
      bmiLabel.text = String(format: "%.02f", bmi!)
    }
  }  
  
  // invokes the method you created with the BMI value and current date
  func saveBMI() {
    if bmi != nil {
      self.healthManager?.saveBMISample(bmi!, date:NSDate())
    } else {
      print("There is no BMI data to save")
    }
  }
  // MARK: - utility methods
  func calculateBMIWithWeightInKilograms(weightInKilograms:Double, heightInMeters:Double) -> Double?
  {
    if heightInMeters == 0 {
      return nil;
    }
    return (weightInKilograms/(heightInMeters*heightInMeters));
  }
  
  func biologicalSexLiteral(biologicalSex:HKBiologicalSex?)->String
  {
    var biologicalSexText = kUnknownString;
    
    if  biologicalSex != nil {
      
      switch( biologicalSex! )
      {
      case .Female:
        biologicalSexText = "Female"
      case .Male:
        biologicalSexText = "Male"
      default:
        break;
      }
      
    }
    return biologicalSexText;
  }
  
  func bloodTypeLiteral(bloodType:HKBloodType?)->String
  {
    
    var bloodTypeText = kUnknownString;
    
    if bloodType != nil {
      
      switch( bloodType! ) {
      case .APositive:
        bloodTypeText = "A+"
      case .ANegative:
        bloodTypeText = "A-"
      case .BPositive:
        bloodTypeText = "B+"
      case .BNegative:
        bloodTypeText = "B-"
      case .ABPositive:
        bloodTypeText = "AB+"
      case .ABNegative:
        bloodTypeText = "AB-"
      case .OPositive:
        bloodTypeText = "O+"
      case .ONegative:
        bloodTypeText = "O-"
      default:
        break;
      }
      
    }
    return bloodTypeText;
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath , animated: true)
    
    switch (indexPath.section, indexPath.row)
    {
    case (UpdateProfileInfoSection,0):
      updateHealthInfo()
    case (SaveBMISection,0):
      saveBMI()
    default:
      break;
    }
    
    
  }
  
}