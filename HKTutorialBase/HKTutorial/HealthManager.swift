//
//  HealthManager.swift
//  HKTutorial
//
//  Created by ernesto on 18/10/14.
//  Copyright (c) 2014 raywenderlich. All rights reserved.
//

import Foundation
import HealthKit

// add all the HealthKit related code for the project; it acts as the gateway for other classes to interact with the HealthKit store - we have an instance of this class in all necessary view controllers, so there is no need to set that up
class HealthManager {
  // core of the HealthKit Framework is this clsas; so we need an instance of this
  let healthKitStore:HKHealthStore = HKHealthStore()
  // request authorization to use HKHealthStore 
  // since the user controls their data, we request access to the specific types of objects our app needs to read/write to the store, rather than requesting global access to the HealthKit store
  // all the obejcts types are subclasses of HKObjectType, and it provides convenience methods to create these subclasses 
  // we only have to invoke one of hte methods with a constant to represent the specific type requested
  /*
  class func quantityTypeForIdentifier(identifier: String!) -> HKQuantityType!  // to get a Quantity Type
  class func categoryTypeForIdentifier(identifier: String!) -> HKCategoryType!  // to get a Category Type
  class func characteristicTypeForIdentifier(identifier: String!) -> HKCharacteristicType! // to get a Characteristic type
  class func correlationTypeForIdentifier(identifier: String!) -> HKCorrelationType! // to get a CorrelationType
  class func workoutType() -> HKWorkoutType! // to get a Workout type
  */
  
  
  /*
  - (void)requestAuthorization {
  
  if ([HKHealthStore isHealthDataAvailable] == NO) {
  // If our device doesn't support HealthKit -> return.
  return;
  }
  
  NSArray *readTypes = @[[HKObjectType characteristicTypeForIdentifier:HKCharacteristicTypeIdentifierDateOfBirth]];
  
  NSArray *writeTypes = @[[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass]];
  
  [self.healthStore requestAuthorizationToShareTypes:[NSSet setWithArray:readTypes]
  readTypes:[NSSet setWithArray:writeTypes] completion:nil];
  }
  */
  func authorizeHealthKit(completion: ((success:Bool, error:NSError!) -> Void)!) {
    // 1) set the types you want to read from HK Store -> creating a set with all the types you need to read from the HealthKit store
    var healthKitTypesToRead = Set<HKObjectType>()
    healthKitTypesToRead.insert(HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierDateOfBirth)!)
    healthKitTypesToRead.insert(HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBloodType)!)
    healthKitTypesToRead.insert(HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBiologicalSex)!)
    healthKitTypesToRead.insert(HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!)
    healthKitTypesToRead.insert(HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!)
    healthKitTypesToRead.insert(HKObjectType.workoutType()) // this one has no subtypes and thus doesn't need an identifier
    
    // 2) set the types you want to write to HK Store -> creating a set with allt he types needed to write to the store
    var healthKitTypesToWrite = Set<HKSampleType>()
    healthKitTypesToWrite.insert(HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMassIndex)!)
    healthKitTypesToWrite.insert(HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!)
    healthKitTypesToWrite.insert(HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!)
    healthKitTypesToWrite.insert(HKQuantityType.workoutType())
    
    // 3) if the store is not available (for instance, iPad), return an error and don't continue
    if !HKHealthStore.isHealthDataAvailable() {
      let error = NSError(domain: "", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available in this Device"])
      if( completion != nil) {
        completion(success:false, error:error)
      }
      return;
    }
    
    // 4) request HealthKit authorization - invokes requestAuthorizationToShareTypes:readTypes with the previously defined types for read and write
    healthKitStore.requestAuthorizationToShareTypes(healthKitTypesToWrite, readTypes: healthKitTypesToRead, completion: { (success, error) -> Void in
      
      if( completion != nil )
      {
        completion(success:success,error:error)
      }
    })
  }
  
  func readProfile() -> (age:Int?, biologicalsex:HKBiologicalSexObject?, bloodtype:HKBloodTypeObject?) {
    var error: NSError?
    var age: Int?
    // 1) Request birthday and calculate age - read the birthday from HKHealthStore, then do a calendar calc to determine age in years
    do { if let birthDay = try healthKitStore.dateOfBirth() as NSDate? {
      let today = NSDate()
      let calendar = NSCalendar.currentCalendar()
      let differenceComponents = calendar.components([.Year], fromDate: birthDay, toDate: today, options: NSCalendarOptions(rawValue: 0))
      age = differenceComponents.year
      }
    }
    catch {
      print("Error reading Birthday: \(error)")
    }
    
    // 2) Read biological sex
    var biologicalSex:HKBiologicalSexObject?
    do {
      biologicalSex = try healthKitStore.biologicalSex() // Determine biological sex
    } catch {
      print("Error reading Biological Sex: \(error)")
    }
    
    // 3) Read blood type 
    var bloodType:HKBloodTypeObject?
    do {
      bloodType = try healthKitStore.bloodType() // Read the blood type
    } catch {
      print("Error reading Blood Type: \(error)")
    }
    
    // 4) Return the information read in a tuple 
    return (age, biologicalSex, bloodType)
  }
  
  func saveBMISample(bmi: Double, date:NSDate) {
    // 1) create a BMI sample by creating a sample object using HKQuantitySample, to create a sample you need
    // a quantity type object, like HKQuantityType, init using the proper sample type
    // a quantity object, like HKQuantity, init using the passed bmi value and the unit - in this case, since BMI is scalar with no units, we need to use a countUnit
    // start and end date, which in this case is the current date and time in both cases
    let bmiType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMassIndex)
    let bmiQuantity = HKQuantity(unit: HKUnit.countUnit(), doubleValue: bmi)
    let bmiSample = HKQuantitySample(type: bmiType!, quantity: bmiQuantity, startDate: date, endDate: date)
    
    // 2) Save the sample in the store - HKHealthStore's method saveObject() is called to add the sample to the HealthKit store
    healthKitStore.saveObject(bmiSample, withCompletion: { (success, error) -> Void in
      if (error != nil) {
        print("Error saving BMI sample: \(error!.localizedDescription)")
      } else {
        print("BMI sample saved successfully!")
      }
    })
  }
  
  // reading data other than characteristics from the store requires a query (HKQuery base class - this is an abstract class with implementations for every type of object. Create a HKSampleQuery in order to read samples. 
  // To build a query, you must 1) specify the sample type you want to query for (ex. weight), 2) use an optional NSPredicate with the search conditions (ex. begin, end date), and an array of NSSortDescriptors telling the store how to order the samples
  // Once we have the query, call the HKHealthStore method executeQuery() to get the results. 
  // Start the query with a generic method that reads the most recent sample of any type. This includes height, weight.
  func readMostRecentSample(sampleType:HKSampleType, completion: ((HKSample!, NSError!) -> Void)!) {
    // 1) Build the predicate based on the date interval by using predicateForSamplesWithStartDate
    let past = NSDate.distantPast() as NSDate
    let now = NSDate()
    let mostRecentPredicate = HKQuery.predicateForSamplesWithStartDate(past, endDate: now, options: .None)
    
    // 2) Build the sort descriptor to return the samples in descending order, ordered by start date
    let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
  
    // 3) We want to limit the number of samples returned by the query to just 1 (the most recent) 
    let limit = 1
    
    // 4) Build samples query by building the query object, using a passed in sample type (the predicate), the sample limit, and the sort descriptor - when the query finishes, it will call the completion closure, where you get the first sample and call your own completion closure with the read value
    let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: mostRecentPredicate, limit: limit, sortDescriptors: nil, resultsHandler: {
      query, results, error in
      if let queryError = error {
        completion(nil, error)
        return;
      }
      
      // Get the first sample
      let mostRecentSample = results!.first as? HKQuantitySample
      
      
      // Execute the completion closure
      if completion != nil {
        completion(mostRecentSample, nil)
      }
    })

    // 5) Execute the query
    self.healthKitStore.executeQuery(sampleQuery)
  }
}